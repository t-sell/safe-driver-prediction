# - tune a decision tree model
#
# load libs, functions file(s)
rm(list = ls())
library(readr)
library(mlr)
library(rpart)

# import data
source(file.path("import-data.R"))

#table(train_df$target)
prop.table(table(train_df$target))
#  0          1 
# 0.96355248 0.03644752 


# create classification task
dtree_task <- makeClassifTask(data = train_df, target = "target", 
                              positive = '1')
target <- getTaskTargets(dtree_task)
tabl <- as.numeric(table(target))
w <- 1/tabl[target]

dtree_model <- train("classif.rpart", task = dtree_task, weights = w)
# -- predict on train
train_preds <- predict(dtree_model, newdata = train_df)
performance(train_preds, measures = list(mmce, acc, ber, tnr, tpr))
# ---

## test_preds <- predict(dtree_model, newdata = test_df)
## 

